document.getElementById('loginForm').addEventListener('submit', function(event) {
    event.preventDefault();
    
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;
    const errorMessage = document.getElementById('error-message');
    
    errorMessage.textContent = '';
    
    if (username === 'admin' && password === 'password1234') {
        window.location.href = 'form.html';
    } else {
        errorMessage.textContent = 'Nom d\'utilisateur ou mot de passe incorrect';
    }
});

if (document.getElementById('submissionForm')) {
    document.getElementById('submissionForm').addEventListener('submit', function(event) {
        event.preventDefault();
        
        const name = document.getElementById('name').value;
        const email = document.getElementById('email').value;
        const message = document.getElementById('message').value;
        const successMessage = document.getElementById('form-success-message');
        
        successMessage.textContent = 'Votre message a été bien reçu !';
        successMessage.classList.remove('hidden');
        
        // Réinitialiser le formulaire après soumission
        document.getElementById('submissionForm').reset();
    });
}

if (document.getElementById('profileForm')) {
    document.getElementById('profileForm').addEventListener('submit', function(event) {
        event.preventDefault();
        
        const fullName = document.getElementById('fullName').value;
        const email = document.getElementById('email').value;
        const bio = document.getElementById('bio').value;
        const profileSuccessMessage = document.getElementById('profile-success-message');
        
        profileSuccessMessage.textContent = 'Votre profil a été mis à jour avec succès !';
        profileSuccessMessage.classList.remove('hidden');
        
        // Réinitialiser le formulaire après soumission
        document.getElementById('profileForm').reset();
    });
}
